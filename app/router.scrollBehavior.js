export default function(to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition
  }

  if (to.hash) {
    const element = document.querySelector(to.hash);
    const navBarOffset = 90;

    // The parent of the element must have css "position:relative"
    if ('scrollBehavior' in document.documentElement.style) {
      return window.scrollTo({ top: element.offsetParent.offsetTop - navBarOffset, behavior: 'smooth' })
    } else {
      return window.scrollTo(0, element.offsetParent.offsetTop - navBarOffset)
    }
  }

  return { x: 0, y: 0 }
}
