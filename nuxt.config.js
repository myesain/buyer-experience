import ignoredFiles from './route.ignore.js'

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',
  generate: {
    // Don't crawl the site links and generate pages - we assume some pages will be built by Middleman or elsewhere.
    crawler: false,
    // To allow the app to redirect to error page
    // https://nuxtjs.org/docs/configuration-glossary/configuration-router/
    fallback: true,
    // Generate routs based on content files
    routes() {
      const glob = require('glob')
      const { uniqBy } = require('lodash')

      return new Promise((resolve, reject) => {
        glob('content/**/*', (err, result) => {
          if (err) {
            return reject(err)
          }

          const filteredResult = result.filter(file => !ignoredFiles.includes(file))
          const routesRegex = /content|\.yml|\/index/mgi

          const routes = uniqBy(filteredResult.map(route => ({ route: route.replace(routesRegex, '') })), 'route')
          resolve(routes)
        })
      })
    }
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    __dangerouslyDisableSanitizersByTagID: {
      schemaOrg: ['innerHTML']
    },
    title: 'GitLab',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'contentSecurityPolicy',
        'http-equiv': 'Content-Security-Policy',
        content: `default-src 'self' https: http:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https: http: ; style-src 'self' 'unsafe-inline' https: http: https://cdn.logrocket.io https://cdn.lr-ingest.io https://cdn.lr-in.com; object-src https: http:; base-uri 'self'; connect-src 'self' https: http: wss: ws: https://*.logrocket.io https://*.lr-ingest.io https://*.logrocket.com https://*.lr-in.com; frame-src 'self' https: http:; img-src 'self' https: http: data:; manifest-src 'self'; media-src 'self' https: http:; child-src 'self' blob: https: http:;`
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Code, test and deploy with GitLab. Everyone can contribute!'
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: 'Code, test and deploy with GitLab. Everyone can contribute!'
      },
      {
        hid: 'og:description',
        name: 'og:description',
        content: 'Code, test and deploy with GitLab. Everyone can contribute!'
      },
      { hid: 'formatDetection', name: 'format-detection', content: 'telephone=no' },
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary_large_image' },
      { hid: 'twitter:site', name: 'twitter:site', content: '@GitLab' },
      {
        hid: 'msapplication-TileImage',
        name: 'msapplication-TileImage',
        content: '/nuxt-images/ico/mstile-144x144.png'
      },
      { hid: 'msapplication-config', name: 'msapplication-config', content: '/nuxt-images/ico/browserconfig.xml' },
      { hid: 'robots', name: 'robots', content: 'index, follow' }
    ],
    link: [
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/nuxt-images/ico/favicon.ico' },
      { rel: 'icon', type: 'image/png', sizes: '192x192', href: '/nuxt-images/ico/favicon-192x192.png' },
      { rel: 'icon', type: 'image/png', sizes: '160x160', href: '/nuxt-images/ico/favicon-160x160.png' },
      { rel: 'icon', type: 'image/png', sizes: '96x96', href: '/nuxt-images/ico/favicon-96x96.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/nuxt-images/ico/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/nuxt-images/ico/favicon-16x16.png' },
      { rel: 'apple-touch-icon', sizes: '57x57', href: '/nuxt-images/ico/apple-touch-icon-57x57.png' },
      { rel: 'apple-touch-icon', sizes: '60x60', href: '/nuxt-images/ico/apple-touch-icon-60x60.png' },
      { rel: 'apple-touch-icon', sizes: '72x72', href: '/nuxt-images/ico/apple-touch-icon-72x72.png' },
      { rel: 'apple-touch-icon', sizes: '76x76', href: '/nuxt-images/ico/apple-touch-icon-76x76.png' },
      { rel: 'apple-touch-icon', sizes: '114x114', href: '/nuxt-images/ico/apple-touch-icon-114x114.png' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: '/nuxt-images/ico/apple-touch-icon-120x120.png' },
      { rel: 'apple-touch-icon', sizes: '144x144', href: '/nuxt-images/ico/apple-touch-icon-144x144.png' },
      { rel: 'apple-touch-icon', sizes: '152x152', href: '/nuxt-images/ico/apple-touch-icon-152x152.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/nuxt-images/ico/apple-touch-icon-180x180.png' },
      { rel: 'alternate', type: 'application/atom+xml', title: 'Blog', href: '/atom.xml' },
      { rel: 'alternate', type: 'application/atom+xml', title: 'All Releases', href: '/all-releases.xml' },
      { rel: 'alternate', type: 'application/atom+xml', title: 'Security Releases', href: '/security-releases.xml' },
      { rel: 'alternate', type: 'application/atom+xml', title: 'Major Releases', href: '/releases.xml' }
    ],
    script: [
      {
        hid: 'schemaOrg',
        innerHTML: `{"@context":"http://schema.org","@type":"Organization","name":"GitLab","legalName":"GitLab Inc.","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}`,
        type: 'application/ld+json'
      },
      {
        hid: 'oneTrustSDK',
        src: 'https://cdn.cookielaw.org/scripttemplates/otSDKStub.js',
        type: 'text/javascript',
        charset: 'utf-8',
        'data-domain-script': '7f944245-c5cd-4eed-a90e-dd955adfdd08'
      },
      {
        hid: 'oneTrustAutoBlocking',
        src: 'https://cdn.cookielaw.org/consent/7f944245-c5cd-4eed-a90e-dd955adfdd08/OtAutoBlock.js',
        type: 'text/javascript'
      },
      {
        hid: 'bizible',
        src: '//cdn.bizible.com/scripts/bizible.js',
        type: 'text/javascript'
      },
      {
        hid: 'munchkin',
        src: '//munchkin.marketo.net/munchkin.js',
        type: 'text/javascript'
      }
    ]
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/normalize',
    '~/assets/css/base'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/slippers-ui.ts',
    { src: '~/plugins/swiftype.js', mode: 'client' },
    { src: '~/plugins/oneTrust.js', mode: 'client' },
    { src: '~/plugins/munchkin.js', mode: 'client' },
    { src: '~/plugins/logrocket.js', mode: 'client' },
    { src: "~/plugins/aos", mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/google-fonts'
  ],

  // nuxt/google-fonts settings: https://google-fonts.nuxtjs.org/
  googleFonts: {
    display: 'swap',
    download: true,
    families: {
      'Source Sans Pro': true
    }
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxt/content',
    '@nuxtjs/gtm',
    '@nuxtjs/markdownit',
    '@nuxtjs/sitemap' // Should be at the end of the array
  ],
  content: {},
  gtm: {
    id: 'GTM-NJXWQL',
    pageTracking: true
  },
  sitemap: {
    path: '/buyer-experience/sitemap.xml',
    hostname: 'https://about.gitlab.com/',
    trailingSlash: true
  },
  markdownit: {
    runtime: true, // Support `$md()`
    linkify: false, // Avoid auto generated hyperlinks (i.e gitlab.com)
    use: [[
      'markdown-it-attrs',
      {
        allowedAttributes: ['data-ga-name', 'data-ga-location']
      }
    ]]
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['slippers-ui']
  }
}
