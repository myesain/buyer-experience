---
  title: "GitLab for Small Business - Collaboration made easy"
  description: "Accelerate your software delivery with GitLab’s DevOps platform, lowering your development costs and streamlining team collaboration"
  ogimage: "https://about.gitlab.com/images/enterprise/open-graph-enterprise.png"
  canonical_url: "/small-business/"
  cta_block:
    title: "Let’s grow together"
    subtitle: "One application, endless possibilities. Our DevOps Platform grows with you."
    free_trial_button:
      text: "Start your free trial"
      link: "https://about.gitlab.com/free-trial/"
      data_ga_name: "free trial"
      data_ga_location: "hero"
    contact_sales_button:
      text: "See pricing"
      link: "https://about.gitlab.com/pricing/"
      data_ga_name: "pricing"
      data_ga_location: "hero"
    tag_line: "Trusted by enterprises, loved by developers."
    showcased_enterprises:
      - image_url: "https://about.gitlab.com/images/organizations/logo_glympse_2.svg"
        alt: "Glympse logo"
      - image_url: "https://about.gitlab.com/images/organizations/logo_hotjar_2.svg"
        alt: "Hotjar logo"
      - image_url: "https://about.gitlab.com/images/organizations/logo_remote_2.svg"
        alt: "remote logo"
      - image_url: "https://about.gitlab.com/images/organizations/logo_trek.svg"
        alt: "trek logo"
      - image_url: "https://about.gitlab.com/images/organizations/logo_mga.svg"
        alt: "mga logo"
    image:
      image_url: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-header-flow-desktop.png"
      alt: Version control

  ship_features_faster_block:
    title: "Leading the way with CI, SCM, and DevSecOps."
    subtitle: "GitLab can be part of your toolchain - or your entire toolchain. We help teams deliver better products faster by deploying as a single application across the entire software development lifecycle."
    image:
      image_url: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-panel-rapid-feedback.png"
      alt: "Image: Gitlab Features"
    content:
      - title: "Single application for the entire DevOps lifecycle"
        description: "GitLab is The DevOps platform, which combines the ability to develop, secure, and operate software in a single application that is easier to use and leads to a faster cycle time."
        icon: "https://about.gitlab.com/images/enterprise/continuous_delivery.svg"
      - title: "Lower cost of development"
        description: "A single application eliminates complex integrations, data chokepoints, and toolchain maintenance, resulting in greater productivity and lower cost."
        icon: "https://about.gitlab.com/images/enterprise/cog_code.svg"
      - title: "Your software, deployed your way"
        description: "GitLab is infrastructure agnostic (supporting GCP, AWS, Azure, OpenShift, VMware, On Prem, Bare Metal, and more), offering a consistent workflow experience - irrespective of the environment."
        icon: "https://about.gitlab.com/images/enterprise/cog_check.svg"

  customer_highlight_block:
    highlighted_customer:
      image:
        image_url: "https://about.gitlab.com/images/small-business/background-news10.jpg"
        alt:
      logo: "https://about.gitlab.com/images/organizations/logo_new10.svg"
      name: "New10"
      impact: "found startup success using GitLab as a single platform for SCM, CI/CD, and AWS integration"
      stats:
        - qualifier: "faster deployments"
          quantifier: "3x+"
          icon: "https://about.gitlab.com/images/icons/icon_up.svg"
        - qualifier: "CI feature branch builds per day"
          quantifier: "20x"
          icon: "https://about.gitlab.com/images/icons/icon_swill.svg"
    customers:
      - image:
          image_url: "https://about.gitlab.com/images/small-business/background-chorus.jpg"
          alt:
        logo: "https://about.gitlab.com/images/organizations/logo_chorus_2.svg"
        name: "Chorus"
        impact: "uses GitLab to integrate development teams"
        icon: "https://about.gitlab.com/images/icons/icon_crown.svg"
        stats:
          - qualifier: "week production cycles"
            quantifier: "1"
          - qualifier: "1"
            quantifier: "click deploys everything"
      - image:
          image_url: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-customer-highlight-hemmersbach.jpg"
          alt:
        logo: "https://about.gitlab.com/images/organizations/logo_glympse_3.svg"
        name: "Glympse"
        impact: "improved security scanning and deployment time with GitLab Ultimate"
        icon: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-icon-case-study-hemmersbach-01.svg"
        stats:
          - qualifier: "tools consolidated"
            quantifier: "20"
          - qualifier: "faster deployments"
            quantifier: "8x"

  feature_list_block:
    title: "How does GitLab help these companies become more productive, efficient and effective?"
    subtitle: "A DevOps platform improves a lot more than the bottom line."
    icon: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-icon-tanuki.svg"
    features:
      - title: "Continuous Integration and Delivery"
        description: "Build high-quality applications at scale with GitLab CI/CD. Accelerate your digital transformation journey, break down department silos, and streamline efficiency."
        icon: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-icon-ci-cd.svg"
        subfeatures:
          - title: "Ensure every code change is releasable"
          - title: "Scale testing with parallel builds and flexible pipelines"
          - title: "Save time with auto-scaling CI/CD job runners"
      - title: "Accelerated software development"
        description: "Delivering software innovation faster can give your organization a competitive advantage, and allow it to truly compete on the digital playing field."
        icon: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-icon-gear.svg"
        subfeatures:
          - title: "Collaborate across team silos"
          - title: "Automate builds and testing"
          - title: "Automate deployment and configuration"
      - title: "Agile Project Management"
        description: "Gain visibility across your organization to deliver on time and on budget. Enable cross-functional collaboration and keep stakeholders connected with kanban boards, epics, and roadmaps."
        icon: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-icon-person-cycle.svg"
        subfeatures:
          - title: "Link issues with code changes"
          - title: "Manage sprints & backlogs"
          - title: "Connect strategy to execution"
      - title: "Source Code Management"
        description: "Deliver better software faster with our enterprise-ready version control and collaboration. Coordinate work, track and review changes, and manage delivery all in one interface."
        icon: "https://about.gitlab.com/images/enterprise/gitlab-enterprise-icon-code.svg"
        subfeatures:
          - title: "Streamline code reviews with live previews"
          - title: "Leverage custom project templates and automated workflows"
          - title: "Manage projects and teams of any size"
