import Vue from 'vue'
import SlippersComponents from 'slippers-ui/dist/slippersComponents.common.js'

// Register Slipper components with Vue
Object.keys(SlippersComponents).forEach(name => {
  Vue.component(name, SlippersComponents[name]);
})
